// Filter click
$("#filterBtn button").click(function(){
     $(this).css({"display":"none"})
    $(".afterClickfilter ").addClass("displayBlock")
})
$(".buttonBox  button").click(function(){
    $("#filterBtn button").css({"display":"flex"})
   $(".afterClickfilter ").removeClass("displayBlock")
})
let vh = window.innerHeight * 0.01;
// Then we set the value in the --vh custom property to the root of the document
document.documentElement.style.setProperty('--vh', `${vh}px`);
window.addEventListener('resize', () => {
    // We execute the same script as before
    let vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
  });